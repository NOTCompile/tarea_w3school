## Introducción
[**Tareas Chingonas W3School**](https://notcompile.gitlab.io/tarea_w3school/ "**Tareas Chingonas W3School**") esta basado en ejemplos proporcionados por [W3School](https://www.w3schools.com/howto/default.asp "W3School"). Este proyecto se realizo con HTML, CSS y JavaScript.

## Instalación
1. Clonar el proyecto subido en GitLab:
	- Repositorio: https://gitlab.com/NOTCompile/tarea_w3school
2. Busca y ejecuta el archivo **index.html** en tu navegador.

## Vista Previa
**Cards**
![](https://notcompile.gitlab.io/tarea_w3school/assets/img/preview_img.JPG)

## Contribución
1. Crea un Fork del Repositorio
2. Clonar en tu PC: https://gitlab.com/NOTCompile/tarea_w3school
3. Crear una nueva rama.
4. Realiza tus cambios.
5. Manda tu pull request.

## Requirimientos para la Contribución
1. Tu dale noma aqui todo se acepta :v, siempre y cuando nos ayude a mejorar.

